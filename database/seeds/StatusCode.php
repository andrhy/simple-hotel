<?php

use Illuminate\Database\Seeder;

class StatusCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_code')->insert([
            ['id' => 1, 'name' => 'Pending'],
            ['id' => 2, 'name' => 'Check In'],
            ['id' => 3, 'name' => 'Check Out'],
            ['id' => 4, 'name' => 'Failed'],
            ['id' => 5, 'name' => 'Cancel'],
        ]);
    }
}
