<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusCode extends Model
{
    protected $table = "status_code";
    protected $dateFormat = 'Y-m-d H:i:s';


}