<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Hotel extends Model implements Auditable
{
    protected $table = "hotel";
    protected $dateFormat = 'Y-m-d H:i:s';
    use \OwenIt\Auditing\Auditable;

    public function jRooms()
    {
        return $this->hasMany('App\Rooms', 'hotel_id', 'id');
    }
}
