<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Hotel;
use App\Rooms;
use App\RoomsOrder;
use App\RoleUsers;
use App\Permission;
use App\Audit;
use Lcobucci\JWT\Parser;
use App\Helpers\ApiResponsesFormat;
use Auth;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    public function login(){
        return json_encode('woww');
    }

    public function postlogin(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if (empty($request->email)) {
                return response()->json($format->formatResponseWithPages("Email required", [], $format->STAT_REQUIRED()));
            } else if (empty($request->password)) {
                return response()->json($format->formatResponseWithPages("Password required", [], $format->STAT_REQUIRED()));
            }

            $checkUser = User::where('email', $request->email)->first();
            if($checkUser){
                if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
                    $user = Auth::user();
                    $data['token'] = 'Bearer '. $user->createToken('Hotel')->accessToken;
                    $data['user']['id'] = $user->id;
                    $data['user']['name'] = $user->name;
                    $data['user']['email'] = $user->email;
                    $data['user']['role'] = $user->getRoles();

                    return response()->json($format->formatResponseWithPages("User Success Login", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Wrong Password", $data, $format->STAT_UNAUTHORIZED()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("User Not Found", $data, $format->STAT_NOT_FOUND()));
            }
            
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }
        /* }  */

    }

    public function postregister(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if (empty($request->name)) {
                return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
            } else if (empty($request->email)) {
                return response()->json($format->formatResponseWithPages("Email required", [], $format->STAT_REQUIRED()));
            } else if (empty($request->password)) {
                return response()->json($format->formatResponseWithPages("Password required", [], $format->STAT_REQUIRED()));
            }

            $checkUser = User::where('email', $request->email)->first();
            if($checkUser){
                return response()->json($format->formatResponseWithPages("Email is already in use", $data, $format->STAT_NOT_FOUND()));
            }else{
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);

                if($user->save()){
                    $member = Role::where('name', 'member')->first();
                    $user->attachRole($member); 
                    if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
                        $userLog = Auth::user();
                        $data['token'] = 'Bearer '. $user->createToken('Hotel')->accessToken;
                        $data['user']['id'] = $userLog->id;
                        $data['user']['name'] = $userLog->name;
                        $data['user']['email'] = $userLog->email;
                        $data['user']['role'] = $user->getRoles();

                        return response()->json($format->formatResponseWithPages("User Success Login", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("User Not Found", $data, $format->STAT_NOT_FOUND()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to create user", [], $format->INTERNAL_SERVER_ERROR()));
                }
                
                
            }
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }


    public function createhotel(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator'])){

                if (empty($request->name)) {
                    return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
                }

                $hotel = new Hotel;
                $hotel->name = $request->name;
                $hotel->description = $request->description;
                if (!empty($request->file('url_image'))) {
                    $destinationPath = 'public/upload/hotel';
                    $path = $request->file('url_image')->storeAs($destinationPath, date('Ymdhis').'_hotel.'.$request->file('url_image')->getClientOriginalExtension());
                    $hotel->url_image = $path;
                } else {
                    $hotel->url_image = '';
                }

                if($hotel->save()){
                    $data['id'] = $hotel->id;
                    $data['name'] = $hotel->name;
                    $data['url_image'] = $hotel->url_image;
                    $data['url_image'] = $hotel->url_image;

                    return response()->json($format->formatResponseWithPages("Success create hotel", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to create hotel", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function updatehotel(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator', 'manager'])){
                if (empty($request->name)) {
                    return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
                }

                $hotel = Hotel::where('id', $id)->first();
                if($hotel){
                    $hotel->name = $request->name;
                    $hotel->description = $request->description;
                    if (!empty($request->file('url_image'))) {
                        $destinationPath = 'public/upload/hotel';
                        $path = $request->file('url_image')->storeAs($destinationPath, date('Ymdhis').'_hotel.'.$request->file('url_image')->getClientOriginalExtension());
                        $hotel->url_image = $path;
                    }
        
                    if($hotel->save()){
                        $data['id'] = $hotel->id;
                        $data['name'] = $hotel->name;
                        $data['url_image'] = $hotel->url_image;
                        $data['url_image'] = $hotel->url_image;
        
                        return response()->json($format->formatResponseWithPages("Success update hotel", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to update hotel", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Hotel not found", [], $format->STAT_NOT_FOUND()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
            
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function deletehotel(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator'])){

                $hotel = Hotel::where('id', $id)->first();
                if($hotel){
        
                    if($hotel->delete()){
                        return response()->json($format->formatResponseWithPages("Success delete hotel", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to delete hotel", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Hotel not found", [], $format->STAT_NOT_FOUND()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }
        /* }  */

    }

    public function showhotel(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
                if(empty($_GET['id'])){
                    $hotels = Hotel::all();
                    foreach($hotels as $key => $hotel){
                        $data[$key]['id'] = $hotel->id;
                        $data[$key]['name'] = $hotel->name;
                        $data[$key]['url_image'] = Storage::url(str_replace("public/", "", $hotel->url_image));
                        $data[$key]['description'] = $hotel->description;
                        foreach($hotel->jRooms as $keys => $room){
                            $data[$key]['room'][$keys]['id'] = $room->id;
                            $data[$key]['room'][$keys]['name'] = $room->name;
                            $data[$key]['room'][$keys]['amount'] = $room->amount;
                        }
                    }
                }else{
                    $hotels = Hotel::where('id', $_GET['id'])->first();
                    if($hotels){
                        $data['id'] = $hotels->id;
                        $data['name'] = $hotels->name;
                        $data['url_image'] = Storage::url(str_replace("public/", "", $hotels->url_image));
                        $data['description'] = $hotels->description;
                        foreach($hotels->jRooms as $keys => $room){
                            $data['room'][$keys]['id'] = $room->id;
                            $data['room'][$keys]['name'] = $room->name;
                            $data['room'][$keys]['amount'] = $room->amount;
                        }
                    }
                }
                
                return response()->json($format->formatResponseWithPages("Show hotel", $data, $format->STAT_OK()));
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function listhotel(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            $hotels = Hotel::all();
            foreach($hotels as $key => $hotel){
                $data[$key]['id'] = $hotel->id;
                $data[$key]['name'] = $hotel->name;
                $data[$key]['url_image'] = 'upload/hotel/1.jpg';//Storage::url(str_replace("public/", "", $hotel->url_image));
                $data[$key]['description'] = $hotel->description;
                foreach($hotel->jRooms as $keys => $room){
                    $data[$key]['room'][$keys]['id'] = $room->id;
                    $data[$key]['room'][$keys]['name'] = $room->name;
                    $data[$key]['room'][$keys]['amount'] = $room->amount;
                    $data[$key]['room'][$keys]['status'] = (RoomsOrder::getAvailable($room->id)) ? 'Available' : 'Full Booked';
                }
            }
                        
            return response()->json($format->formatResponseWithPages("List hotel", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function detailhotel($id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            $hotel = Hotel::where('id', $id)->first();
            if($hotel){
                $data['id'] = $hotel->id;
                $data['name'] = $hotel->name;
                $data['url_image'] = 'upload/hotel/1.jpg'; //Storage::url(str_replace("public/", "", $hotel->url_image));
                $data['description'] = $hotel->description;
                foreach($hotel->jRooms as $keys => $room){
                    $data['room'][$keys]['id'] = $room->id;
                    $data['room'][$keys]['name'] = $room->name;
                    $data['room'][$keys]['amount'] = $room->amount;
                    $data['room'][$keys]['available'] = RoomsOrder::getAvailable($room->id);
                }
            }
                        
            return response()->json($format->formatResponseWithPages("Detail hotel", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function createrooms(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator'])){
                $getHotel = Hotel::where('id', $request->hotel_id)->first();
                if (empty($request->name)) {
                    return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->hotel_id)) {
                    return response()->json($format->formatResponseWithPages("Hotel required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->amount)) {
                    return response()->json($format->formatResponseWithPages("Amount required", [], $format->STAT_REQUIRED()));
                }else if(!$getHotel){
                    return response()->json($format->formatResponseWithPages("Hotel Not Found", [], $format->STAT_NOT_FOUND()));
                }

                $rooms = new Rooms;
                $rooms->name = $request->name;
                $rooms->hotel_id = $request->hotel_id;
                $rooms->amount = $request->amount;
                
                if($rooms->save()){
                    $data['id'] = $rooms->id;
                    $data['name'] = $rooms->name;
                    $data['hotel_id'] = $rooms->hotel_id;
                    $data['amount'] = $rooms->amount;

                    return response()->json($format->formatResponseWithPages("Success create rooms", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to create rooms", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function updaterooms(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator', 'manager'])){
                $getHotel = Hotel::where('id', $request->hotel_id)->first();
                if (empty($request->name)) {
                    return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->hotel_id)) {
                    return response()->json($format->formatResponseWithPages("Hotel required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->amount)) {
                    return response()->json($format->formatResponseWithPages("Amount required", [], $format->STAT_REQUIRED()));
                }else if(!$getHotel){
                    return response()->json($format->formatResponseWithPages("Hotel Not Found", [], $format->STAT_NOT_FOUND()));
                }

                $rooms = Rooms::where('id', $id)->first();
                if($rooms){
                    $rooms->name = $request->name;
                    $rooms->hotel_id = $request->hotel_id;
                    $rooms->amount = $request->amount;
        
                    if($rooms->save()){
                        $data['id'] = $rooms->id;
                        $data['name'] = $rooms->name;
                        $data['hotel_id'] = $rooms->hotel_id;
                        $data['amount'] = $rooms->amount;
        
                        return response()->json($format->formatResponseWithPages("Success update rooms", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to update rooms", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Rooms not found", [], $format->STAT_NOT_FOUND()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }    
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function deleterooms(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator'])){
                $rooms = Rooms::where('id', $id)->first();
                if($rooms){
        
                    if($rooms->delete()){
                        return response()->json($format->formatResponseWithPages("Success delete rooms", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to delete rooms", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Rooms not found", [], $format->STAT_NOT_FOUND()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }
        /* }  */

    }

    public function showrooms(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            if(empty($_GET['id'])){
                $rooms = Rooms::all();
                foreach($rooms as $key => $room){
                    $data[$key]['id'] = $room->id;
                    $data[$key]['name'] = $room->name;
                    $data[$key]['hotel_id'] = $room->hotel_id;
                    $data[$key]['hotel_name'] = $room->jHotels->name;
                    $data[$key]['amount'] = $room->amount;
                }
            }else{
                $rooms = Rooms::where('id', $_GET['id'])->first();
                if($rooms){
                    $data['id'] = $rooms->id;
                    $data['name'] = $rooms->name;
                    $data['hotel_id'] = $rooms->hotel_id;
                    $data['hotel_name'] = $rooms->jHotels->name;
                    $data['amount'] = $rooms->amount;
                }
            }
            
            return response()->json($format->formatResponseWithPages("Show Rooms", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function createusers(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator'])){

                if (empty($request->name)) {
                    return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
                } else if (empty($request->email)) {
                    return response()->json($format->formatResponseWithPages("Email required", [], $format->STAT_REQUIRED()));
                } else if (empty($request->password)) {
                    return response()->json($format->formatResponseWithPages("Password required", [], $format->STAT_REQUIRED()));
                } else if (empty($request->role)) {
                    return response()->json($format->formatResponseWithPages("Role required", [], $format->STAT_REQUIRED()));
                }

                $checkUser = User::where('email', $request->email)->first();
                if($checkUser){
                    return response()->json($format->formatResponseWithPages("Email is already in use", $data, $format->STAT_NOT_FOUND()));
                }else{
                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = bcrypt($request->password);

                    if($user->save()){
                        $member = Role::where('name', $request->role)->first();
                        $user->attachRole($member); 
                        $data['id'] = $user->id;
                        $data['name'] = $user->name;
                        $data['email'] = $user->email;
                        $data['role'] = $request->role;
        
                        return response()->json($format->formatResponseWithPages("Success create user", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to create user", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                    
                    
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function updateusers(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if (empty($request->name)) {
                return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
            } else if (empty($request->email)) {
                return response()->json($format->formatResponseWithPages("Email required", [], $format->STAT_REQUIRED()));
            } else if (empty($request->role)) {
                return response()->json($format->formatResponseWithPages("Role required", [], $format->STAT_REQUIRED()));
            }

            $checkUser = User::where('email', $request->email)->first();
            if(!$checkUser && ($checkUser->id != $id)){
                return response()->json($format->formatResponseWithPages("Email is already in use", $data, $format->STAT_NOT_FOUND()));
            }
            
            $user = User::where('id', $id)->first();
            if($user){
                $user->name = $request->name;
                $user->email = $request->email;
    
                if($user->save()){
                    $detach = Role::all()->pluck('id')->toArray();
                    $user->detachRoles($detach);
                    $member = Role::where('name', $request->role)->first();
                    $user->attachRole($member); 
                    $data['id'] = $user->id;
                    $data['name'] = $user->name;
                    $data['email'] = $user->email;
                    $data['role'] = $request->role;
    
                    return response()->json($format->formatResponseWithPages("Success update user", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to update user", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("User not found", $data, $format->STAT_NOT_FOUND()));

            }

        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function updatepassword(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if (empty($request->password)) {
                return response()->json($format->formatResponseWithPages("Password required", [], $format->STAT_REQUIRED()));
            }
            
            $user = User::where('id', $id)->first();
            if($user){
                $user->password = bcrypt($request->password);
    
                if($user->save()){
                    $data['id'] = $user->id;
                    $data['name'] = $user->name;
                    $data['email'] = $user->email;
    
                    return response()->json($format->formatResponseWithPages("Success update password", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to update password", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("User not found", $data, $format->STAT_NOT_FOUND()));

            }

        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function deleteusers(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator'])){
                $user = User::where('id', $id)->first();
                if($user){
        
                    $detach = Role::all()->pluck('id')->toArray();
                    $user->detachRoles($detach);
                    if($user->delete()){
                        return response()->json($format->formatResponseWithPages("Success delete users", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to delete users", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("User not found", $data, $format->STAT_NOT_FOUND()));

                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }

        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function showusers(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            if(Auth::user()->hasRole(['administrator'])){

                if(empty($_GET['id'])){
                    $users = User::all();
                    foreach($users as $key => $user){
                        $data[$key]['id'] = $user->id;
                        $data[$key]['name'] = $user->name;
                        $data[$key]['email'] = $user->email;
                        $data[$key]['role'] = $user->getRoles();
                    }
                }else{
                    $users = User::where('id', $_GET['id'])->first();
                    if($users){
                        $data['id'] = $users->id;
                        $data['name'] = $users->name;
                        $data['email'] = $users->email;
                        $data['role'] = $users->getRoles();
                    }
                }
                
                return response()->json($format->formatResponseWithPages("Show Users", $data, $format->STAT_OK()));
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }


    public function updateprofile(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if (empty($request->name)) {
                return response()->json($format->formatResponseWithPages("Name required", [], $format->STAT_REQUIRED()));
            } else if (empty($request->email)) {
                return response()->json($format->formatResponseWithPages("Email required", [], $format->STAT_REQUIRED()));
            }

            $checkUser = User::where('email', $request->email)->first();
            if(!$checkUser && ($checkUser->id != Auth::user()->id)){
                return response()->json($format->formatResponseWithPages("Email is already in use", $data, $format->STAT_NOT_FOUND()));
            }
            
            $user = User::where('id', Auth::user()->id)->first();
            if($user){
                $user->name = $request->name;
                $user->email = $request->email;
    
                if($user->save()){
                    $data['id'] = $user->id;
                    $data['name'] = $user->name;
                    $data['email'] = $user->email;
    
                    return response()->json($format->formatResponseWithPages("Success update profile", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to update profile", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("User not found", $data, $format->STAT_NOT_FOUND()));

            }

        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function getprofile(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            $users = User::where('id', Auth::user()->id)->first();
            if($users){
                $data['id'] = $users->id;
                $data['name'] = $users->name;
                $data['email'] = $users->email;
                $data['role'] = $users->getRoles();
            }
            
            return response()->json($format->formatResponseWithPages("Get Profile", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }


    public function updatepass(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            
            $user = User::where('id', Auth::user()->id)->first();
            if($user){
                $user->password = bcrypt($request->password);
                if($user->save()){
                    return response()->json($format->formatResponseWithPages("Success update password", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to update password", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("User not found", $data, $format->STAT_NOT_FOUND()));

            }

        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function createroomsorder(Request $request){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator', 'member'])){
                $getRoom = Rooms::where('id', $request->room_id)->first();
                if (empty($request->room_id)) {
                    return response()->json($format->formatResponseWithPages("Rooms required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->checkin)) {
                    return response()->json($format->formatResponseWithPages("Check In required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->checkout)) {
                    return response()->json($format->formatResponseWithPages("Check Out required", [], $format->STAT_REQUIRED()));
                }else if (date('Y-m-d', strtotime($request->checkin)) > date('Y-m-d', strtotime($request->checkout))) {
                    return response()->json($format->formatResponseWithPages("Check In is greater than Check Out", [], $format->STAT_REQUIRED()));
                }else if(!$getRoom){
                    return response()->json($format->formatResponseWithPages("Room Not Found", [], $format->STAT_REQUIRED()));
                }

                $rooms = new RoomsOrder;
                $rooms->user_id = $request->user_id;
                $rooms->room_id = $request->room_id;
                $rooms->checkin =  date('Y-m-d', strtotime($request->checkin));
                $rooms->checkout =  date('Y-m-d', strtotime($request->checkout));
                $rooms->status = 1;
                
                if($rooms->save()){
                    $data['id'] = $rooms->id;
                    $data['user_id'] = $rooms->user_id;
                    $data['room_id'] = $rooms->room_id;
                    $data['checkin'] = $rooms->checkin;
                    $data['checkout'] = $rooms->checkout;
                    $data['status'] = $rooms->jStatusCode->name;

                    return response()->json($format->formatResponseWithPages("Success create order rooms", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to create order rooms", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function updateroomsorder(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator', 'manager', 'member'])){
                $getRoom = Rooms::where('id', $request->room_id)->first();
                if (empty($request->room_id)) {
                    return response()->json($format->formatResponseWithPages("Rooms required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->checkin)) {
                    return response()->json($format->formatResponseWithPages("Check In required", [], $format->STAT_REQUIRED()));
                }else if (empty($request->checkout)) {
                    return response()->json($format->formatResponseWithPages("Check Out required", [], $format->STAT_REQUIRED()));
                }else if (date('Y-m-d', strtotime($request->checkin)) > date('Y-m-d', strtotime($request->checkout))) {
                    return response()->json($format->formatResponseWithPages("Check In is greater than Check Out", [], $format->STAT_REQUIRED()));
                }else if(!$getRoom){
                    return response()->json($format->formatResponseWithPages("Room Not Found", [], $format->STAT_REQUIRED()));
                }

                $rooms = RoomsOrder::where('id', $id)->first();
                if($rooms){

                    $rooms->room_id = $request->room_id;
                    $rooms->checkin =  date('Y-m-d', strtotime($request->checkin));
                    $rooms->checkout =  date('Y-m-d', strtotime($request->checkout));
                    if(!empty($request->status)){
                        $rooms->status = $request->status;
                    }
                    if($rooms->save()){
                        $data['id'] = $rooms->id;
                        $data['user_id'] = $rooms->user_id;
                        $data['room_id'] = $rooms->room_id;
                        $data['checkin'] = $rooms->checkin;
                        $data['checkout'] = $rooms->checkout;
                        $data['user_name'] = $rooms->user_name;
                        $data['status'] = $rooms->jStatusCode->name;
        
                        return response()->json($format->formatResponseWithPages("Success update order rooms", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to update order rooms", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Order Not Found", [], $format->STAT_NOT_FOUND()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function updateroomsorderstatus(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if($rooms){
                $rooms = RoomsOrder::where('id', $id)->first();

                $rooms->status = $request->status;
                if($rooms->save()){
                    $data['id'] = $rooms->id;
                    $data['user_id'] = $rooms->user_id;
                    $data['room_id'] = $rooms->room_id;
                    $data['checkin'] = $rooms->checkin;
                    $data['checkout'] = $rooms->checkout;
                    $data['user_name'] = $rooms->user_name;
                    $data['status'] = $rooms->jStatusCode->name;
    
                    return response()->json($format->formatResponseWithPages("Success update order rooms", $data, $format->STAT_OK()));
                }else{
                    return response()->json($format->formatResponseWithPages("Failed to update order rooms", [], $format->INTERNAL_SERVER_ERROR()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Order Not Found", [], $format->STAT_NOT_FOUND()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function deleteroomsorder(Request $request, $id){

        $format = new ApiResponsesFormat();

        $data = [];
        try{
            if(Auth::user()->hasRole(['administrator', 'manager'])){
                $rooms = RoomsOrder::where('id', $id)->first();
                if($rooms){

                    if($rooms->delete()){
        
                        return response()->json($format->formatResponseWithPages("Success delete order rooms", $data, $format->STAT_OK()));
                    }else{
                        return response()->json($format->formatResponseWithPages("Failed to delete order rooms", [], $format->INTERNAL_SERVER_ERROR()));
                    }
                }else{
                    return response()->json($format->formatResponseWithPages("Order Not Found", [], $format->STAT_NOT_FOUND()));
                }
            }else{
                return response()->json($format->formatResponseWithPages("Access Denied", [], $format->STAT_UNAUTHORIZED()));
            }
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function showroomsorder(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            if(empty($_GET['id'])){
                $rooms = RoomsOrder::all();
                foreach($rooms as $key => $room){
                    $data[$key]['id'] = $room->id;
                    $data[$key]['user_id'] = $room->user_id;
                    $data[$key]['user_name'] = empty($room->jUsers) ? '' : $room->jUsers->name;
                    $data[$key]['room_id'] = $room->room_id;
                    $data[$key]['room_name'] = $room->jRooms->name;
                    $data[$key]['hotel_name'] = $room->jRooms->jHotels->name;
                    $data[$key]['checkin'] = $room->checkin;
                    $data[$key]['checkout'] = $room->checkout;
                    $data[$key]['order_date'] = date('Y-m-d', strtotime( $room->created_at));
                    $data[$key]['status'] = $room->jStatusCode->name;
                    $data[$key]['status_code'] = $room->status;
                }
            }else{
                $room = RoomsOrder::where('id', $_GET['id'])->first();
                if($room){
                    $data['id'] = $room->id;
                    $data['user_id'] = $room->user_id;
                    $data['user_name'] = empty($room->jUsers) ? '' : $room->jUsers->name;
                    $data['room_id'] = $room->room_id;
                    $data['room_name'] = $room->jRooms->name;
                    $data['hotel_name'] = $room->jRooms->jHotels->name;
                    $data['checkin'] = $room->checkin;
                    $data['checkout'] = $room->checkout;
                    $data['order_date'] = date('Y-m-d', strtotime( $room->created_at));
                    $data['status'] = $room->jStatusCode->name;
                    $data['status_code'] = $room->status;

                }
            }
            
            return response()->json($format->formatResponseWithPages("Show Rooms Order", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function showmyorder(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            if(empty($_GET['id'])){
                $rooms = RoomsOrder::where('user_id', Auth::user()->id)->get();
                foreach($rooms as $key => $room){
                    $data[$key]['id'] = $room->id;
                    $data[$key]['user_id'] = $room->user_id;
                    $data[$key]['user_name'] = empty($room->jUsers) ? '' : $room->jUsers->name;
                    $data[$key]['room_id'] = $room->room_id;
                    $data[$key]['room_name'] = $room->jRooms->name;
                    $data[$key]['hotel_name'] = $room->jRooms->jHotels->name;
                    $data[$key]['checkin'] = $room->checkin;
                    $data[$key]['checkout'] = $room->checkout;
                    $data[$key]['order_date'] = date('Y-m-d', strtotime( $room->created_at));
                    $data[$key]['status'] = $room->jStatusCode->name;
                }
            }else{
                $room = RoomsOrder::where('id', $_GET['id'])->first();
                if($room){
                    $data['id'] = $room->id;
                    $data['user_id'] = $room->user_id;
                    $data['user_name'] = empty($room->jUsers) ? '' : $room->jUsers->name;
                    $data['room_id'] = $room->room_id;
                    $data['room_name'] = $room->jRooms->name;
                    $data['hotel_name'] = $room->jRooms->jHotels->name;
                    $data['checkin'] = $room->checkin;
                    $data['checkout'] = $room->checkout;
                    $data['order_date'] = date('Y-m-d', strtotime( $room->created_at));
                    $data['status'] = $room->jStatusCode->name;
                }
            }
            
            return response()->json($format->formatResponseWithPages("Show Rooms Order", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function getaudit(){

        $format = new ApiResponsesFormat();

        $data = [];
        try{

            $audits = Audit::all();
            foreach($audits as $key => $audit){
                $data[$key]['id'] = $audit->id;
                $data[$key]['name'] = $audit->jUsers->name;
                $data[$key]['auditable_type'] = $audit->auditable_type;
                $data[$key]['event'] = $audit->event;
                $data[$key]['new_values'] = $audit->new_values;
            }
            
            return response()->json($format->formatResponseWithPages("Get Audit", $data, $format->STAT_OK()));
                
        }catch(Exception $e){
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }

    }

    public function logout(Request $request){
        try {
            /* $token = $request->user()->tokens->find(Auth::user()->id);
            $token->revoke(); */

            // helper format JSON
            $format = new ApiResponsesFormat();

            // result Logout TRUE
            return response()->json($format->formatResponseWithPages("Logout success", [], $format->STAT_OK()));
        } catch (Exception $e) {

            //return error message
            return response()->json($format->formatResponseWithPages("Internal Server Error", [], $format->INTERNAL_SERVER_ERROR()));
        }
    }

    public function callbackgoogle(){
        $user = Socialite::driver('google')->user();
    }

    public function usersall(){
        $users = User::all();

        return json_encode($users);
    }

}
