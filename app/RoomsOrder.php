<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RoomsOrder extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = "rooms_order";
    protected $dateFormat = 'Y-m-d H:i:s';

    public function jStatusCode()
    {
        return $this->belongsTo('App\StatusCode', 'status', 'id');
    }

    public function jUsers()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function jRooms()
    {
        return $this->belongsTo('App\Rooms', 'room_id', 'id');
    }

    public static function getAvailable($id){
        $getOrder = RoomsOrder::where('room_id', $id)->whereIn('status', [1,2])->get();
        if($getOrder->isEmpty()){
            return true;
        }else{
            $amount = Rooms::where('id', $id)->first();
            if($getOrder->count() >= (int)$amount->amount){
                return false;
            }else{
                return true;
            }
        }
    }
}
