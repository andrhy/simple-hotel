<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Rooms extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = "rooms";
    protected $dateFormat = 'Y-m-d H:i:s';

    public function jHotels()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }

}
