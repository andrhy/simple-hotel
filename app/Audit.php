<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = "audits";
    protected $dateFormat = 'Y-m-d H:i:s';

    public function jUsers()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
