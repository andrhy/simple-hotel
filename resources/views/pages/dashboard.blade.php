@extends('layouts.main')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

<div id="app">
    <dashboard></dashboard>
</div>

@stop