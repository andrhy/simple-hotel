@extends('layouts.front')

@section('content')

<div id="app">
<header-login></header-login>
<div class="container-fluid h-100">
        
    <my-account></my-account>
    
</div>
    
</div>
@stop