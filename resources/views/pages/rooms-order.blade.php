@extends('layouts.front')

@section('content')

<div id="app">
<header-login></header-login>
<div class="container-fluid h-100">
        
    <rooms-order></rooms-order>
    
</div>
    
</div>
@stop