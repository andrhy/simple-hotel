@extends('layouts.main')

@section('content_header')
    <h1>Users</h1>
@stop
@section('content')

<div id="app">
    <users-component></users-component>
</div>
@stop