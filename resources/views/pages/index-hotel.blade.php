@extends('layouts.main')

@section('content_header')
    <h1>Hotels</h1>
@stop
@section('content')

<div id="app">
    <hotels-component></hotels-component>
</div>
@stop