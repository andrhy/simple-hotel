@extends('layouts.main')

@section('content_header')
    <h1>Add Hotel</h1>
@stop
@section('content')

<div id="app">
    <hotels-add></hotels-add>
</div>
@stop