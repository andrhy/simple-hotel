@extends('layouts.main')

@section('content_header')
    <h1>Rooms</h1>
@stop
@section('content')

<div id="app">
    <room-component></room-component>
</div>
@stop