@extends('layouts.main')

@section('content_header')
    <h1>Add Room</h1>
@stop
@section('content')

<div id="app">
    <room-add></room-add>
</div>
@stop