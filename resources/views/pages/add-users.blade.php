@extends('layouts.main')

@section('content_header')
    <h1>Add User</h1>
@stop
@section('content')

<div id="app">
    <users-add></users-add>
</div>
@stop