@extends('layouts.front')

@section('content')

<div id="app">
<header-login></header-login>
<div class="container-fluid h-100">
        
    <detail-hotel></detail-hotel>
    
</div>
    
</div>
@stop