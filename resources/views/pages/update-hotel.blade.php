@extends('layouts.main')

@section('content_header')
    <h1>Update Hotel</h1>
@stop
@section('content')

<div id="app">
    <hotels-update></hotels-update>
</div>
@stop