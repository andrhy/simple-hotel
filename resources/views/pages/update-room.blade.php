@extends('layouts.main')

@section('content_header')
    <h1>Update Room</h1>
@stop
@section('content')

<div id="app">
    <room-update></room-update>
</div>
@stop