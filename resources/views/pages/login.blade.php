@extends('layouts.login')

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )
@php( $dashboard_url = View::getSection('dashboard_url') ?? config('adminlte.dashboard_url', 'home') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
@endif

@section('content')
    <div class="wrapper h-100">

        <div class="row w-100" style="position: absolute;top: 35%;">
            <div class="col-12">
                <div class="login-logo">
                    <a href="{{ $dashboard_url }}">Please Login To Continue</a>
                </div>
            </div>
            <div class="col-12" >
                    <div class="row">
                        <div class="col-6" style="border-right: 1px solid #333;">
                            <users-login></users-login>
                            
                        </div>

                        <div class="col-6">
                            <users-register></users-register>
                        </div>
                    </div>

            </div>
        </div>
        

        
    </div>
@stop

@section('js')
    <script src="{{ asset('js/app.js') }}"></script>
@stop