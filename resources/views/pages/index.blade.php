@extends('layouts.front')

@section('content')

<div id="app">
<header-login></header-login>
<div class="container-fluid h-100">

    <div class="row w-100" style="position: absolute;top: 25%;">
        <div class="col-12">
            <div class="login-logo">
                <a href="#">Welcome to Hotel Booking System</a>
            </div>
        </div>
        <index-order></index-order>
        
    </div>
    
</div>
    
</div>
@stop