/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import Datepicker from 'vuejs-datepicker';

window.Vue.use(VueRouter);
window.Vue.use(Datepicker);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('users-component', require('./components/users/UsersIndex.vue').default);
Vue.component('hotels-component', require('./components/hotels/IndexHotel.vue').default);
Vue.component('room-component', require('./components/rooms/IndexRoom.vue').default);
Vue.component('order-component', require('./components/order/IndexOrder.vue').default);
Vue.component('users-login', require('./components/users/UsersLogin.vue').default);
Vue.component('users-register', require('./components/users/UsersRegister.vue').default);
Vue.component('index-order', require('./components/roomsorder/IndexOrder.vue').default);
Vue.component('header-login', require('./components/roomsorder/HeaderLogin.vue').default);
Vue.component('detail-hotel', require('./components/roomsorder/DetailHotel.vue').default);
Vue.component('rooms-order', require('./components/roomsorder/RoomsOrder.vue').default);
Vue.component('my-order', require('./components/roomsorder/MyOrder.vue').default);
Vue.component('my-account', require('./components/users/MyAccount.vue').default);

Vue.component('dashboard', require('./components/dashboard/IndexDashboard.vue').default);

Vue.component('users-add', require('./components/users/AddUsers.vue').default);
Vue.component('users-update', require('./components/users/UpdateUsers.vue').default);
Vue.component('callback-google', require('./components/users/Callback.vue').default);

Vue.component('hotels-add', require('./components/hotels/AddHotel.vue').default);
Vue.component('hotels-update', require('./components/hotels/UpdateHotel.vue').default);

Vue.component('room-add', require('./components/rooms/AddRoom.vue').default);
Vue.component('room-update', require('./components/rooms/UpdateRoom.vue').default);

Vue.component('order-add', require('./components/order/AddOrder.vue').default);
Vue.component('order-update', require('./components/order/UpdateOrder.vue').default);

Vue.component('audit-log', require('./components/dashboard/Audit.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const routes = [
    {
        path: '/login',
        name: 'login'
    }
]

const router = new VueRouter({ mode: 'history', routes })

const app = new Vue({ router, Datepicker }).$mount('#app')