<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use App\Role;

Route::get('/', function () {
    return view('pages.index');
});

Route::get('/login', function () {
    return view('pages.login');
});

Route::get('/detail-hotel/{id}', function () {
    return view('pages.detail-hotel');
});

Route::get('/order-rooms/{id}', function () {
    return view('pages.rooms-order');
});

Route::get('/my-order', function () {
    return view('pages.my-order');
});

Route::get('/my-account', function () {
    return view('pages.my-account');
});

Route::get('/dashboard', function () {
    return view('pages.dashboard');
});

Route::get('/dashboard/users', function () {
    return view('pages.index-users');
});

Route::get('/dashboard/users/add', function () {
    return view('pages.add-users');
});

Route::get('/dashboard/users/update/{id}', function () {
    return view('pages.update-users');
});

Route::get('/dashboard/hotels', function () {
    return view('pages.index-hotel');
});

Route::get('/dashboard/hotels/add', function () {
    return view('pages.add-hotels');
});

Route::get('/dashboard/hotels/update/{id}', function () {
    return view('pages.update-hotel');
});

Route::get('/dashboard/rooms', function () {
    return view('pages.index-room');
});

Route::get('/dashboard/rooms/add', function () {
    return view('pages.add-room');
});

Route::get('/dashboard/rooms/update/{id}', function () {
    return view('pages.update-room');
});

Route::get('/dashboard/orders', function () {
    return view('pages.index-order');
});

Route::get('/dashboard/order/add', function () {
    return view('pages.add-order');
});

Route::get('/dashboard/order/update/{id}', function () {
    return view('pages.update-order');
});

Route::get('/dashboard/account', function () {
    return view('pages.dashboard-account');
});

Route::get('/dashboard/audit', function () {
    return view('pages.audit');
});

Route::get('/callback/google', function () {
    $userGoogle = Socialite::driver('google')->stateless()->user();
    $token = '';

    $checkUser = User::where('email', $userGoogle->getEmail())->first();
    if($checkUser){
        $checkUser->provider_id = $userGoogle->getId();
        if($checkUser->save()){
            $token = 'Bearer '. $checkUser->createToken('Hotel')->accessToken;
        }
    }else{
        $user = new User;
        $user->name = $userGoogle->getName();
        $user->email = $userGoogle->getEmail();
        $user->provider_id = $userGoogle->getId();
        $user->password = bcrypt($userGoogle->getId());

        if($user->save()){
            $member = Role::where('name', 'member')->first();
            $user->attachRole($member);
            $token = 'Bearer '. $user->createToken('Hotel')->accessToken;
        }       
        
    }
    return view('pages.callback', ['token' => $token]);
});

Route::get('/redirect/google', function () {
    return Socialite::driver('google')->redirect();

});

/* Route::group(['prefix' => ''], function () {
    Route::get('/login', ['as' => 'login.index', 'uses' => 'PageController@login']);

    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('/', ['as' => 'dashboard.index', 'uses' => 'PageController@index']);

    });

}); */