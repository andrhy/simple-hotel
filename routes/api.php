<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/api', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', ['as' => 'api.post.login.index', 'uses' => 'ApiController@postlogin']);
    Route::post('/register', ['as' => 'api.post.register.index', 'uses' => 'ApiController@postregister']);
    Route::get('/hotel/list', ['as' => 'api.hotel.list.index', 'uses' => 'ApiController@listhotel']);
    Route::get('/hotel/detail/{id}', ['as' => 'api.hotel.detail.index', 'uses' => 'ApiController@detailhotel']);
    
    Route::get('/callback/google', ['as' => 'api.hotel.detail.index', 'uses' => 'ApiController@callbackgoogle']);

    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('/logout', ['as' => 'api.logout.index', 'uses' => 'ApiController@logout']);
        
        Route::get('/users/all', ['as' => 'api.users.all.index', 'uses' => 'ApiController@usersall']);

        /* hotel */
        Route::post('/hotel/create', ['as' => 'api.hotel.create.index', 'uses' => 'ApiController@createhotel']);
        Route::post('/hotel/update/{id}', ['as' => 'api.hotel.create.index', 'uses' => 'ApiController@updatehotel']);
        Route::post('/hotel/delete/{id}', ['as' => 'api.hotel.create.index', 'uses' => 'ApiController@deletehotel']);
        Route::get('/hotel/show', ['as' => 'api.hotel.create.index', 'uses' => 'ApiController@showhotel']);

        /* rooms */
        Route::post('/rooms/create', ['as' => 'api.rooms.create.index', 'uses' => 'ApiController@createrooms']);
        Route::post('/rooms/update/{id}', ['as' => 'api.rooms.create.index', 'uses' => 'ApiController@updaterooms']);
        Route::post('/rooms/delete/{id}', ['as' => 'api.rooms.create.index', 'uses' => 'ApiController@deleterooms']);
        Route::get('/rooms/show', ['as' => 'api.rooms.create.index', 'uses' => 'ApiController@showrooms']);

        /* users */
        Route::post('/users/create', ['as' => 'api.users.create.index', 'uses' => 'ApiController@createusers']);
        Route::post('/users/update/{id}', ['as' => 'api.users.update.index', 'uses' => 'ApiController@updateusers']);
        Route::post('/users/update/password/{id}', ['as' => 'api.users.update.password.index', 'uses' => 'ApiController@updatepassword']);
        Route::post('/users/delete/{id}', ['as' => 'api.users.create.index', 'uses' => 'ApiController@deleteusers']);
        Route::get('/users/show', ['as' => 'api.users.create.index', 'uses' => 'ApiController@showusers']);
        
        /* profil */
        Route::post('/update/profile', ['as' => 'api.update.profile.index', 'uses' => 'ApiController@updateprofile']);
        Route::post('/update/password', ['as' => 'api.update.password.index', 'uses' => 'ApiController@updatepass']);
        Route::get('/get/profile', ['as' => 'api.get.profile.index', 'uses' => 'ApiController@getprofile']);
        
        /* rooms order */
        Route::post('/rooms-order/create', ['as' => 'api.rooms.order.create.index', 'uses' => 'ApiController@createroomsorder']);
        Route::post('/rooms-order/update/{id}', ['as' => 'api.rooms.order.update.index', 'uses' => 'ApiController@updateroomsorder']);
        Route::post('/rooms-order/update/status/{id}', ['as' => 'api.rooms.order.update.status.index', 'uses' => 'ApiController@updateroomsorderstatus']);
        Route::post('/rooms-order/delete/{id}', ['as' => 'api.rooms.create.index', 'uses' => 'ApiController@deleteroomsorder']);
        Route::get('/rooms-order/show', ['as' => 'api.rooms.create.index', 'uses' => 'ApiController@showroomsorder']);
        Route::get('/my-order', ['as' => 'api.my.order.index', 'uses' => 'ApiController@showmyorder']);
        
        Route::get('/get/audit', ['as' => 'api.get.audit.index', 'uses' => 'ApiController@getaudit']);
    });

});